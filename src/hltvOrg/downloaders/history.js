const request = require('superagent');

const downloadHistory = (team, from, to) => {
  const statsURL = `https://www.hltv.org/stats/teams/matches/${team.id}/${team.slug}?startDate=${from}&endDate=${to}`;

  return request
    .get(statsURL)
    .retry(3)
    .then((res) => res.text);
};

module.exports = downloadHistory;