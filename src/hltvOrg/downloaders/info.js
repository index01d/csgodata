const request = require('superagent');

const downloadInfo = (team, from, to) => {
  const statsURL = `https://www.hltv.org/team/${team.id}/${team.slug}`;

  return request
    .get(statsURL)
    .retry(3)
    .then((res) => res.text);
};

module.exports = downloadInfo;