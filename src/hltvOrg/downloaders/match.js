const request = require('superagent');

const downloadMatch = (URL) => {
  const id = URL.split('/')[4];
  const slug = URL.split('/')[5];
  
  return request
    .get(URL)
    .retry(3)
    .then((res) => ({ id, slug, html: res.text }));
};

module.exports = downloadMatch;