const request = require('superagent');

const downloadStats = (team, from, to) => {
  const statsURL = `https://www.hltv.org/stats/teams/maps/${team.id}/${team.slug}?startDate=${from}&endDate=${to}`;

  return request
    .get(statsURL)
    .retry(3)
    .then((res) => res.text);
};

module.exports = downloadStats;