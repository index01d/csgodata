const cheerio = require('cheerio');

const extractHistory = (html) => {
  const $ = cheerio.load(html);
  const matchNodes = $('.stats-table tbody tr');

  return matchNodes.map((i, el) => ({
    date: $(el).find('.time a').text(),
    event: $(el).find('a.image-and-label span').text(),
    opponent: $(el).find('.flag').parent().text(),
    map: $(el).find('.statsMapPlayed span').text(),
    win: $(el).children().last().text() == 'W'
  })).get();
};

module.exports = extractHistory;