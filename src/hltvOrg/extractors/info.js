const cheerio = require('cheerio');

const extractInfo = (html) => {
  const $ = cheerio.load(html);

  return {
    rank: $('.profile-team-stat span a').text().replace('#', '')
  };
};

module.exports = extractInfo;