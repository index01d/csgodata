const cheerio = require('cheerio');

const moment = require('moment');

const extractMatch = ({ id, slug, html }) => {
  $ = cheerio.load(html);

  const team1Node = $('.team1-gradient a');
  const team2Node = $('.team2-gradient a');

  const team1score = $('.team1-gradient div') && $('.team1-gradient').children('div').text();
  const team2score = $('.team1-gradient div') && $('.team2-gradient').children('div').text();

  const dateNode = $('.date');

  return {
    id,
    slug,

    date: moment.unix(dateNode.attr('data-unix')/1000),

    team1: {
      id:   team1Node.attr('href').split('/')[2],
      slug: team1Node.attr('href').split('/')[3],
      name: team1Node.text().trim(),
      score: team1score
    },

    team2: {
      id:   team2Node.attr('href').split('/')[2],
      slug: team2Node.attr('href').split('/')[3],
      name: team2Node.text().trim(),
      score: team2score
    }
  };
}

module.exports = extractMatch;