const cheerio = require('cheerio');

const extractPlayers = (html) => {
  const $ = cheerio.load(html);

  const playerNodes = $('.player-ratings-table tbody tr');



  return {
    players: playerNodes
               .map(
                 (i, el) => ({
                   nickname: $(el).find('.playerCol a').text(),
                   kd: $(el).find('.statsDetail').eq(1).text(),
                   rating: $(el).find('.ratingCol').text()
                 })
               ).get()
  };
};

module.exports = extractPlayers;