const cheerio = require('cheerio');
const { isEmpty } = require('lodash');

const extractStats = (html) => {
  $ = cheerio.load(html);

  const mapNodes = $('.stats-team-maps .two-grid')
                      .not('.win-defeat-container')
                      .children('div.col')
                      .not((i, el) => isEmpty($(el).text()));

  const mapNode2rows = (mapNode) => {
    const rows = {};
    
    $(mapNode)
      .find('.stats-rows')
      .children('.stats-row')
      .map((i, row) => Object.assign(
        rows, 
        {
          [$(row).children('span').first().text()]: $(row).children('span').eq(1).text()
        }
      ));

    return rows;
  };
  
  return {
    maps: mapNodes.map(
      (i, mapNode) => (Object.assign(
        {
          name: $(mapNode).find('.map-pool-map-name').text()
        },
        mapNode2rows(mapNode)
      ))   
    ).get()
  }
};

module.exports = extractStats;