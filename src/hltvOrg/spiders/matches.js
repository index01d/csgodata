const request = require('superagent');

const cheerio = require('cheerio');

const { flatten, map } = require('lodash');

const listURL = ({ startDate, endDate, offset = 0, stars }) => {
  let result = `https://www.hltv.org/results?startDate=${startDate}&endDate=${endDate}`;
  
  if (stars)
    result += `&stars=${stars}`;

  if (offset > 0)
    result += `&offset=${offset}`;

  return result;
};

const crawlMatches = ({ startDate, endDate }) => {
  return request
    .get(listURL({ startDate, endDate }))
    .then((res) => res.text)
    .then(
      (html) => {
        const $ = cheerio.load(html);

        const paginationArray = $('.pagination-data').text().split(' ');
        
        let total = 1;
        if (paginationArray.length > 1) 
          total = Math.round(paginationArray[paginationArray.length - 2] / 100);

        const pages = [];
        
        for (let i = 0; i < total; i++)
          pages.push(listURL({ startDate, endDate, offset: i * 100, stars: 1 }));

        return pages;
      }
    )
    .then(
      (pages) => (
        Promise.all(
          pages.map((page) => 
            request
              .get(page)
              .then((res) => res.text)
              .then(
                (html) => {
                  const $ = cheerio.load(html);
          
                  const matches = $('.result-con .a-reset').map((i, el) => $(el).attr('href')).get();
          
                  return matches;
                }
              )
          )
        )
      )
    )
    .then(flatten)
    .then((items) => map(items, (url) => `https://www.hltv.org${url}`));
};

module.exports = crawlMatches;