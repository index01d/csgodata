const moment = require('moment');

const transformHistory = (history) => {
  const matches = history.map(
    (match) => Object.assign(match, {
      date: moment(match.date, 'DD-MM-YY')
    })
  );

  const latest = matches.slice(0, 10);

  return {
    last10WinRate: latest.filter((match) => match.win).length / 10  
  };
};

module.exports = transformHistory;