const { isEmpty } = require('lodash');

const transformInfo = ({ rank }) => {
  return {
    rank: isEmpty(rank) ? 1000 : rank
  };
};

module.exports = transformInfo;