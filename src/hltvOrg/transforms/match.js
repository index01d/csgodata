const { round, uniq, concat, toNumber, intersection, reduce } = require('lodash');

const math = require('mathjs');

const transformMatch = (match) => {
  const stats1 = match.team1.stats;
  const stats2 = match.team2.stats;

  const goodMaps1 = stats1.maps.filter((map) => map.winRate > 0.5);
  const goodMaps2 = stats2.maps.filter((map) => map.winRate > 0.5);

  const uniqueMaps = uniq(concat([], stats1.maps.map((map) => map.name), stats2.maps.map((map) => map.name)));

  const mapWinRate1 = goodMaps1.map((map) => map.winRate);
  const mapWinRate2 = goodMaps2.map((map) => map.winRate);

  return {
    team1: match.team1.name,
    team2: match.team2.name,
    goodMapDiff: goodMaps1.length - goodMaps2.length, // Разница количества карт с рейтом побед > 50% за последние 3 месяца
    goodMapDiffRate: round(goodMaps1.length / goodMaps2.length, 2) || 1, // Отношение количества карт с рейтом побед > 50% за последние 3 месяца
    last10diff: round(stats1.last10WinRate / stats2.last10WinRate, 2) || 1, // Отношение рейта побед в последних десяти играх
    totalMapsDiff: round(
      reduce(stats1.maps.map((map) => map.total), (acc, value) => acc + value, 0) / 
      reduce(stats2.maps.map((map) => map.total), (acc, value) => acc + value, 0), 
      2
    ) || 0, // Отношение количества сыгранных карт
    mapWinRateDiff: (mapWinRate1.length > 0 ? math.mean(mapWinRate1) : 0.5) / (mapWinRate2.length > 0 ? math.mean(mapWinRate2) : 0.5),
    intersection: round(intersection(
      goodMaps1.map((map) => map.name), 
      goodMaps2.map((map) => map.name)
    ).length / uniqueMaps.length, 2) || 0, // Процент пересекающихся хороших карт
    rankDiff: stats1.rank - stats2.rank, // Расстояние в общемировом рейтинге
    kdDiff: stats1.avgKD / stats2.avgKD, // Разница в среднем KD
    ratingDiff: stats1.avgRating / stats2.avgRating, // Разница в среднем рейтинге
    win: toNumber(match.team1.score) > toNumber(match.team2.score) ? 1 : 0// Победила ли команда 1
  };
};

module.exports = transformMatch;