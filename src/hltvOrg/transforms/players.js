const { isEmpty, concat } = require('lodash');

const math = require('mathjs');

const transformPlayers = ({ players }) => {
  const kds = players.map((player) => Number(player.kd));
  const ratings = players.map((player) => Number(player.rating));
  const avgKD = kds.length > 0 ? math.mean(kds) : 1;
  const avgRating = ratings.length > 0 ? math.mean(ratings) : 1;
  return {
    avgKD: avgKD > 0 ? avgKD : 1,
    avgRating: avgRating > 0 ? avgRating : 1
  };
};

module.exports = transformPlayers;