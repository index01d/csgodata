const { trim, toNumber, round } = require('lodash');

const transformStats = (stats) => ({
  maps: stats.maps.map(
    (mapStat) => {
      const WinsDrawsLosses = mapStat['Wins / draws / losses']
                                .split('/')
                                .map(trim)
                                .map(toNumber);
      
      return ({
        name:    mapStat['name'],
        wins:    WinsDrawsLosses[0],
        losses:  WinsDrawsLosses[1] + WinsDrawsLosses[2],
        total:   WinsDrawsLosses.reduce((accumulator, value) => accumulator + value),
        rounds:  toNumber(mapStat['Total rounds']),
        winRate: round(toNumber(mapStat['Win rate'].replace('%','')) / 100, 3)
      })
    }
  )
});

module.exports = transformStats;