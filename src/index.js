const path = require('path');
require('app-module-path').addPath(path.join(process.cwd(), 'src'));

const moment = require('moment');

const downloadMatch = require('hltvOrg/downloaders/match');
const downloadStats = require('hltvOrg/downloaders/stats');
const downloadHistory = require('hltvOrg/downloaders/history');
const downloadInfo = require('hltvOrg/downloaders/info');
const downloadPlayers = require('hltvOrg/downloaders/players');

const extractMatch = require('hltvOrg/extractors/match');
const extractStats = require('hltvOrg/extractors/stats');
const extractHistory = require('hltvOrg/extractors/history');
const extractInfo = require('hltvOrg/extractors/info');
const extractPlayers = require('hltvOrg/extractors/players');

const transformStats = require('hltvOrg/transforms/stats');
const transformHistory = require('hltvOrg/transforms/history');
const transformMatch = require('hltvOrg/transforms/match');
const transformInfo = require('hltvOrg/transforms/info');
const transformPlayers = require('hltvOrg/transforms/players');

const spider = require('hltvOrg/spiders/matches');

const processMatch = (match) => {
  const from = match.date.clone().subtract(3, 'months').format('YYYY-MM-DD');
  const to   = match.date.clone().subtract(1, 'day'   ).format('YYYY-MM-DD');

  return new Promise(
    (resolve, reject) => {
      Promise.all([
        downloadStats(match.team1, from, to)
          .then(extractStats)
          .then(transformStats),
        downloadStats(match.team2, from, to)
          .then(extractStats)
          .then(transformStats),
        downloadHistory(match.team1, from, to)
          .then(extractHistory)
          .then(transformHistory),
        downloadHistory(match.team2, from, to)
          .then(extractHistory)
          .then(transformHistory),
        downloadInfo(match.team1, from, to)
          .then(extractInfo)
          .then(transformInfo),
        downloadInfo(match.team2, from, to)
          .then(extractInfo)
          .then(transformInfo),
        downloadPlayers(match.team1, from, to)
          .then(extractPlayers)
          .then(transformPlayers),
        downloadPlayers(match.team2, from, to)
          .then(extractPlayers)
          .then(transformPlayers)
      ]).then(values => resolve(
        Object.assign(
          {},
          match,
          {
            team1: Object.assign(
              {}, 
              match.team1, 
              { 
                stats: Object.assign(values[0], values[2], values[4], values[6])
              }
            ),
            team2: Object.assign(
              {}, 
              match.team2, 
              { 
                stats: Object.assign(values[1], values[3], values[5], values[7])
              }
            ),
          }
        )
      ))
    }
  );
};

const match2dataset = (URL) => {
  const { omit, values } = require('lodash');

  return downloadMatch(URL)
    .then(extractMatch)
    .then(processMatch)
    .then(transformMatch)
    .then((match) => values(match).join(','));
};

async function run() {
  const fs = require('fs');
  const stream = fs.createWriteStream("dataset.csv");
  stream.once('open', async (fd) => {
    stream.write('team1,team2,goodMapDiff,goodMapDiffRate,last10diff,totalMapsDiff,mapWinRateDiff,intersection,rankDiff,kdDiff,ratingDiff,win\n');

    const pages = await spider({ startDate: '2018-01-01', endDate: '2018-04-23' });

    console.log(`Crawled ${pages.length} matches URL`);

    let estimate = pages.length;
    
    for(let i = 0; i < pages.length; i++) {
      try {
        const page = pages[i];
        const result = await match2dataset(page);
        console.log(estimate);
        
        estimate = estimate - 1;
        stream.write(`${result}\n`);
      }
      catch (e) { console.log('Fatal Error: ', e); }
    }
  }); 
};

// run();

const urls = [
  'https://www.hltv.org/matches/2319956/fnatic-vs-natus-vincere-esl-pro-league-season-7-europe',
  'https://www.hltv.org/matches/2322233/nip-vs-mousesports-ecs-season-5-europe',
  'https://www.hltv.org/matches/2321430/g2-vs-fnatic-ecs-season-5-europe',
  'https://www.hltv.org/matches/2321431/fnatic-vs-g2-ecs-season-5-europe'
];

for (url of urls)
  match2dataset(url)
    .then(console.log);